import discord
from discord.ext import commands
from time import time
from discord_slash import SlashContext
from discord_slash.client import SlashCommand



def HasRole(ctx: commands.Context, item):

    roles = ctx.author.roles
    
    if isinstance(item, int):
        role = discord.utils.get(roles, id = item)
    else:
        role = discord.utils.get(roles, name = item)

    if role == None:
        return False
    else:
        return True


#cooldown system :D

data = dict()
def cooldown(ctx, delay: int):

    id = 0
    if(isinstance(id, SlashContext)): #just incase probably not being used tbh.
        id = ctx.author_id
    else:
        id = ctx.author.id

    if len(data) == 0:
        data.update({f'{id}': delay})
        return True, 0

    current_delay = data.get(f'{id}')
    if current_delay != None:
        if round(time()) > current_delay:
            data.pop(f'{id}')
            return True, 0
        else:
            return False, current_delay - round(time())
    else:
        data.update({f'{id}': delay})
        return True, 0