# XDA Helper - Development Branch
We do some development and other fun stuff here 

# XDA Helper - Main Branch
For documentation, refer to the beta branch

## Credits
- @NullCode13 for initial project
- @AnishDe12020 for patches and features
- @Otus9051 bug fixes
- @NabsiYa for module system

## Contribute
If you want to contribute, make your PR's at `beta` branch.
